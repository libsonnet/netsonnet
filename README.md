Network jsonnet libraries
----
Netsonnet provides several tools to manage networks through Jsonnet, natively. 

## Getting started

### Prerequisites

Because `netsonnet` is a Jsonnet library, [jsonnet](https://jsonnet.org/) is required to use it.  
If you want to contribute to this project, you also need to install [`jsonnet-bundler`](github.com/jsonnet-bundler/jsonnet-bundler). 

> :warning: the current release is 0.2.0, but this library requires a later version (integrating go-like import path), so it is mandatory to get this tools from master.  
> `GO111MODULE="on" go get github.com/jsonnet-bundler/jsonnet-bundler/cmd/jb@master`

### Install netsonnet

You have several way to download this library:
- **[jsonnet-bundler](github.com/jsonnet-bundler/jsonnet-bundler):** `jb install https://gitlab.com/libsonnet/netsonnet`
- **git:** `git clone https://gitlab.com/libsonnet/netsonnet.git`
- **curl**
  - *ipv4 library:* `curl -o ipv4.libsonnet https://gitlab.com/libsonnet/netsonnet/raw/master/ipv4.libsonnet` 
- **wget**
  - *ipv4 library:* `wget https://gitlab.com/libsonnet/netsonnet/raw/master/ipv4.libsonnet`

Then import the library in your jsonnet code. For example, with jsonnet-bundler:

```
local ipv4 = import 'netsonnet/ipv4.libsonnet'
```

To be able to find the netsonnet library, you must pass the folder where are the downloaded files using the -J option. For example, with jsonnet-bundler:

```
jsonnet -J vendor your-file.jsonnet
```

### Examples

Equivalent to run the `ipcalc command` with the ipv4 libraries:

```jsonnet
local ipv4 = import 'ipv4.libsonnet';

local addr = ipv4.addr(std.extVar('address'));
local subnet = ipv4.subnet(addr);

local atob = function(bytes)
  if bytes < 1 then ''
  else atob(std.floor(bytes / 2)) + bytes % 2;
local displayBits = function(bytes)
  local bits = atob(bytes);
  local padding0 = function(x) if x == 0 then '' else padding0(x - 1) + '0';
  local chunk = function(x, i)
    if i == 0 then []
    else if i % 8 == 0 then [std.substr(x, 0, 8)] + chunk(std.substr(x, 8, std.length(x)), i - 1)
    else chunk(x, i - 1);

  std.join('.', chunk(padding0(32 - std.length(bits)) + bits, 32));
local padding = function(str, x)
  if x == 0 then str
  else padding(' ' + str, x - 1);

// > jsonnet ipcalc.jsonnet -V address="1.1.1.1/24" -S
//
// Address:   1.1.1.1              00000001.00000001.00000001.00000001
// Netmask:   255.255.255.0 = 24   11111111.11111111.11111111.00000000
// Wildcard:  0.0.0.255            00000000.00000000.00000000.11111111
// =>
// Network:   1.1.1.0/24           00000001.00000001.00000001.00000000
// HostMin:   1.1.1.1              00000001.00000001.00000001.00000001
// HostMax:   1.1.1.254            00000001.00000001.00000001.11111110
// Broadcast: 1.1.1.255            00000001.00000001.00000001.11111111
// Hosts/Net: 254

if addr.mask == 32 then
  'Address:   %s%s\nNetmask:   %s = %s%s\nWildcard:  %s%s\n=>\nHostroute: %s%s\nHosts/Net: %d' % [
    addr.short,
    padding(displayBits(addr.bytes), 21 - std.length(addr.short)),
    subnet.netmask,
    subnet.mask,
    padding(displayBits(ipv4.addr(subnet.netmask).bytes), 21 - std.length('%s = %s' % [subnet.netmask, subnet.mask])),
    subnet.wildcard,
    padding(displayBits(ipv4.addr(subnet.wildcard).bytes), 21 - std.length(subnet.wildcard)),
    subnet.short,
    padding(displayBits(ipv4.addr(subnet.address).bytes), 21 - std.length(subnet.address)),
    subnet.numHost,
  ]
else if addr.mask == 31 then
  'Address:   %s%s\nNetmask:   %s = %s%s\nWildcard:  %s%s\n=>\nNetwork:   %s%s\nHostMin:   %s%s\nHostMax:   %s%s\nHosts/Net: %d' % [
    addr.short,
    padding(displayBits(addr.bytes), 21 - std.length(addr.short)),
    subnet.netmask,
    subnet.mask,
    padding(displayBits(ipv4.addr(subnet.netmask).bytes), 21 - std.length('%s = %s' % [subnet.netmask, subnet.mask])),
    subnet.wildcard,
    padding(displayBits(ipv4.addr(subnet.wildcard).bytes), 21 - std.length(subnet.wildcard)),
    subnet.address,
    padding(displayBits(ipv4.addr(subnet.address).bytes), 21 - std.length(subnet.address)),
    subnet.hostMin,
    padding(displayBits(ipv4.addr(subnet.hostMin).bytes), 21 - std.length(subnet.hostMin)),
    subnet.hostMax,
    padding(displayBits(ipv4.addr(subnet.hostMax).bytes), 21 - std.length(subnet.hostMax)),
    subnet.numHost,
  ]
else
  'Address:   %s%s\nNetmask:   %s = %s%s\nWildcard:  %s%s\n=>\nNetwork:   %s%s\nHostMin:   %s%s\nHostMax:   %s%s\nBroadcast: %s%s\nHosts/Net: %d' % [
    addr.short,
    padding(displayBits(addr.bytes), 21 - std.length(addr.short)),
    subnet.netmask,
    subnet.mask,
    padding(displayBits(ipv4.addr(subnet.netmask).bytes), 21 - std.length('%s = %s' % [subnet.netmask, subnet.mask])),
    subnet.wildcard,
    padding(displayBits(ipv4.addr(subnet.wildcard).bytes), 21 - std.length(subnet.wildcard)),
    subnet.address,
    padding(displayBits(ipv4.addr(subnet.address).bytes), 21 - std.length(subnet.address)),
    subnet.hostMin,
    padding(displayBits(ipv4.addr(subnet.hostMin).bytes), 21 - std.length(subnet.hostMin)),
    subnet.hostMax,
    padding(displayBits(ipv4.addr(subnet.hostMax).bytes), 21 - std.length(subnet.hostMax)),
    subnet.broadcast,
    padding(displayBits(ipv4.addr(subnet.broadcast).bytes), 21 - std.length(subnet.broadcast)),
    subnet.numHost,
  ]
```