local test = import 'github.com/yugui/jsonnetunit/jsonnetunit/test.libsonnet';
local ipv4 = import 'ipv4.libsonnet';

local addr = ipv4.addr('1.1.1.1/13');
local subnet = ipv4.subnet('1.1.1.1/13');

local debug = function(x) std.trace('debug: ' + x, x);
test.suite({
  // ipv4.isAddrObject
  testisAddrObject_true: { actual: ipv4.isAddrObject(addr), expect: true },
  testisAddrObject_withSubnet_true: { actual: ipv4.isAddrObject(subnet), expect: true },
  testisAddrObject_false: { actual: ipv4.isAddrObject('1.1.1.1/13'), expect: false },

  // ipv4.addr
  testAddr_string: { actual: ipv4.addr('1.1.1.1/13').full, expect: '1.1.1.1/13' },
  testAddr_mixed: { actual: ipv4.addr('1.1.1.1', addr.mask).full, expect: '1.1.1.1/13' },
  testAddr_int: { actual: ipv4.addr(addr.bytes, addr.mask).full, expect: '1.1.1.1/13' },

  'testAddr_0.0.0.0/0_short': { actual: ipv4.addr('0.0.0.0/0').short, expect: '0.0.0.0' },
  'testAddr_0.0.0.0/0_full': { actual: ipv4.addr('0.0.0.0/0').full, expect: '0.0.0.0/0' },
  'testAddr_0.0.0.0/0_mask': { actual: ipv4.addr('0.0.0.0/0').mask, expect: 0 },
  'testAddr_0.0.0.0/0_bytes': { actual: ipv4.addr('0.0.0.0/0').bytes, expect: 0 },

  'testAddr_255.255.255.255/32_short': { actual: ipv4.addr('255.255.255.255/32').short, expect: '255.255.255.255' },
  'testAddr_255.255.255.255/32_full': { actual: ipv4.addr('255.255.255.255/32').full, expect: '255.255.255.255/32' },
  'testAddr_255.255.255.255/32_mask': { actual: ipv4.addr('255.255.255.255/32').mask, expect: 32 },
  'testAddr_255.255.255.255/32_bytes': { actual: ipv4.addr('255.255.255.255/32').bytes, expect: 4294967295 },

  // ipv4.subnet
  'testSubnet_192.168.1.0/31': {
    actual: ipv4.subnet('192.168.1.0/31'),
    expect: { address: '192.168.1.0/31', broadcast: null, hostMax: '192.168.1.1', hostMin: '192.168.1.0', mask: 31, netmask: '255.255.255.254', numHost: 2, wildcard: '0.0.0.1' },
  },
  'testSubnet_127.0.0.1/32': {
    actual: ipv4.subnet('127.0.0.1/32'),
    expect: { address: '127.0.0.1/32', broadcast: null, hostMax: '127.0.0.1', hostMin: '127.0.0.1', mask: 32, netmask: '255.255.255.255', numHost: 1, wildcard: '0.0.0.0' },
  },
  'testSubnet_0.0.0.0/0': {
    actual: ipv4.subnet('0.0.0.0/0'),
    expect: { address: '0.0.0.0/0', broadcast: '255.255.255.255', hostMax: '255.255.255.254', hostMin: '0.0.0.1', mask: 0, netmask: '0.0.0.0', numHost: 4294967294, wildcard: '255.255.255.255' },
  },
  'testSubnet_255.255.255.255/32': {
    actual: ipv4.subnet('255.255.255.255/32'),
    expect: { address: '255.255.255.255/32', broadcast: null, hostMax: '255.255.255.255', hostMin: '255.255.255.255', mask: 32, netmask: '255.255.255.255', numHost: 1, wildcard: '0.0.0.0' },
  },

  // ipv4.subnet with random IP in subnet
  'testAddress_216.58.209.227/22': {
    actual: ipv4.subnet('216.58.209.227/22'),
    expect: { address: '216.58.208.0/22', broadcast: '216.58.211.255', hostMax: '216.58.211.254', hostMin: '216.58.208.1', mask: 22, netmask: '255.255.252.0', numHost: 1022, wildcard: '0.0.3.255' },
  },
  'testAddress_216.58.209.227/11': {
    actual: ipv4.subnet('216.58.209.227/11'),
    expect: { address: '216.32.0.0/11', broadcast: '216.63.255.255', hostMax: '216.63.255.254', hostMin: '216.32.0.1', mask: 11, netmask: '255.224.0.0', numHost: 2097150, wildcard: '0.31.255.255' },
  },

  // ipv4.subnet fields
  'testField_192.168.1.0/24_address': {
    actual: ipv4.subnet('192.168.1.0/24').address,
    expect: '192.168.1.0/24',
  },
  'testField_192.168.1.0/24_mask': {
    actual: ipv4.subnet('192.168.1.0/24').mask,
    expect: 24,
  },
  'testField_192.168.1.0/24_netmask': {
    actual: ipv4.subnet('192.168.1.0/24').netmask,
    expect: '255.255.255.0',
  },
  'testField_192.168.1.0/24_wildcard': {
    actual: ipv4.subnet('192.168.1.0/24').wildcard,
    expect: '0.0.0.255',
  },
  'testField_192.168.1.0/24_broadcast': {
    actual: ipv4.subnet('192.168.1.0/24').broadcast,
    expect: '192.168.1.255',
  },
  'testField_192.168.1.0/24_hostMax': {
    actual: ipv4.subnet('192.168.1.0/24').hostMax,
    expect: '192.168.1.254',
  },
  'testField_192.168.1.0/24_hostMin': {
    actual: ipv4.subnet('192.168.1.0/24').hostMin,
    expect: '192.168.1.1',
  },
  'testField_192.168.1.0/24_numHost': {
    actual: ipv4.subnet('192.168.1.0/24').numHost,
    expect: 254,
  },

  // ipv4.subnet.(next|prev)
  'testSubnet_10.0.0.0/8': {
    actual: ipv4.subnet('10.0.0.0/8'),
    expect: { address: '10.0.0.0/8', broadcast: '10.255.255.255', hostMax: '10.255.255.254', hostMin: '10.0.0.1', mask: 8, netmask: '255.0.0.0', numHost: 16777214, wildcard: '0.255.255.255' },
  },
  'testSubnet_10.0.0.0/8_prev': {
    actual: ipv4.subnet('10.0.0.0/8').prev(),
    expect: { address: '9.0.0.0/8', broadcast: '9.255.255.255', hostMax: '9.255.255.254', hostMin: '9.0.0.1', mask: 8, netmask: '255.0.0.0', numHost: 16777214, wildcard: '0.255.255.255' },
  },
  'testSubnet_10.0.0.0/8_next': {
    actual: ipv4.subnet('10.0.0.0/8').next(),
    expect: { address: '11.0.0.0/8', broadcast: '11.255.255.255', hostMax: '11.255.255.254', hostMin: '11.0.0.1', mask: 8, netmask: '255.0.0.0', numHost: 16777214, wildcard: '0.255.255.255' },
  },
  'testSubnet_10.0.0.0/8_next_5': {
    actual: ipv4.subnet('10.0.0.0/8').next(5),
    expect: { address: '15.0.0.0/8', broadcast: '15.255.255.255', hostMax: '15.255.255.254', hostMin: '15.0.0.1', mask: 8, netmask: '255.0.0.0', numHost: 16777214, wildcard: '0.255.255.255' },
  },

  // ipv4.subnet.ip
  'testSubnet_10.0.0.0/8_ip': {
    actual: ipv4.subnet('10.0.0.0/8').ip().full,
    expect: '10.0.0.0/8',
  },
  'testSubnet_10.0.0.0/8_ip_5': {
    actual: ipv4.subnet('10.0.0.0/8').ip(5).full,
    expect: '10.0.0.5/8',
  },
  'testSubnet_10.0.0.0/8_ip_1234': {
    actual: ipv4.subnet('10.0.0.0/8').ip(1234).full,
    expect: '10.0.4.210/8',
  },
  'testSubnet_10.0.0.0/30_ip_1': {
    actual: ipv4.subnet('10.0.0.0/30').ip(1).full,
    expect: '10.0.0.1/30',
  },

  // ipv4.subnet.hasSubnet
  'testSubnet_10.0.0.0/8_hasSubnet_10.0.1.0/24': {
    actual: ipv4.subnet('10.0.0.0/8').hasSubnet('10.0.1.0/24'),
    expect: true,
  },
  'testSubnet_10.0.0.0/8_hasSubnet_9.0.1.0/24': {
    actual: ipv4.subnet('10.0.0.0/8').hasSubnet('9.0.1.0/24'),
    expect: false,
  },
  'testSubnet_10.0.0.0/8_hasSubnet_10.0.0.0/7': {
    actual: ipv4.subnet('10.0.0.0/8').hasSubnet('10.0.0.0/7'),
    expect: false,
  },
  'testSubnet_addrVar_hasSubnet_1.1.1.1/24': {
    actual: ipv4.subnet(addr).hasSubnet('1.1.1.1/24'),
    expect: true,
  },
  'testSubnet_addrVar_hasSubnet_10.0.0.0/7': {
    actual: ipv4.subnet(addr).hasSubnet('10.0.0.0/7'),
    expect: false,
  },
  'testSubnet_subnetVar_hasSubnet_1.1.1.1/24': {
    actual: ipv4.subnet(subnet).hasSubnet('1.1.1.1/24'),
    expect: true,
  },
  'testSubnet_subnetVar_hasSubnet_10.0.0.0/7': {
    actual: ipv4.subnet(subnet).hasSubnet('10.0.0.0/7'),
    expect: false,
  },

  // ipv4.subnet.numSubnet
  'testSubnet_10.0.0.0/8_numSubnet_/8': {
    actual: ipv4.subnet('10.0.0.0/8').numSubnet(8),
    expect: 1,
  },
  'testSubnet_10.0.0.0/8_numSubnet_/10': {
    actual: ipv4.subnet('10.0.0.0/8').numSubnet(10),
    expect: 4,
  },
  'testSubnet_10.0.0.0/8_numSubnet_/24': {
    actual: ipv4.subnet('10.0.0.0/8').numSubnet(24),
    expect: 65536,
  },

  // ipv4.subnet.divide
  'testSubnet_10.0.0.0/8_divide_/24': {
    actual: ipv4.subnet('10.0.0.0/8').divide(24),
    expect: { address: '10.0.0.0/24', broadcast: '10.0.0.255', hostMax: '10.0.0.254', hostMin: '10.0.0.1', mask: 24, netmask: '255.255.255.0', numHost: 254, wildcard: '0.0.0.255' },
  },
  'testSubnet_10.0.0.0/8_divide_/24_next': {
    actual: ipv4.subnet('10.0.0.0/8').divide(24).next(),
    expect: { address: '10.0.1.0/24', broadcast: '10.0.1.255', hostMax: '10.0.1.254', hostMin: '10.0.1.1', mask: 24, netmask: '255.255.255.0', numHost: 254, wildcard: '0.0.0.255' },
  },
  'testSubnet_10.0.0.0/4_divide_/16': {
    actual: ipv4.subnet('10.0.0.0/4').divide(16),
    expect: { address: '0.0.0.0/16', broadcast: '0.0.255.255', hostMax: '0.0.255.254', hostMin: '0.0.0.1', mask: 16, netmask: '255.255.0.0', numHost: 65534, wildcard: '0.0.255.255' },
  },
  'testSubnet_10.0.0.0/4_divide_/16_next_8': {
    actual: ipv4.subnet('10.0.0.0/4').divide(16).next(8),
    expect: { address: '0.8.0.0/16', broadcast: '0.8.255.255', hostMax: '0.8.255.254', hostMin: '0.8.0.1', mask: 16, netmask: '255.255.0.0', numHost: 65534, wildcard: '0.0.255.255' },
  },

  // ipv4.subnet chaining
  'testChaining_192.168.1.0/24_next_next2_next2_prev4': {
    actual: ipv4.subnet('192.168.1.0', 24).next().next(2).next(2).prev(4).address,
    expect: '192.168.2.0/24',
  },

  // ipv4.subnet variables
  'testVariable_1.1.1.1/13_address': {
    actual: subnet.address,
    expect: '1.0.0.0/13',
  },
  'testVariable_1.1.1.1/13_next_address': {
    actual: subnet.next().address,
    expect: '1.8.0.0/13',
  },
})
