local const = {
  pow2:: [1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536, 131072, 262144, 524288, 1048576, 2097152, 4194304, 8388608, 16777216, 33554432, 67108864, 134217728, 268435456, 536870912, 1073741824, 2147483648, 4294967296],
  mask:: [0, 2147483648, 3221225472, 3758096384, 4026531840, 4160749568, 4227858432, 4261412864, 4278190080, 4286578688, 4290772992, 4292870144, 4293918720, 4294443008, 4294705152, 4294836224, 4294901760, 4294934528, 4294950912, 4294959104, 4294963200, 4294965248, 4294966272, 4294966784, 4294967040, 4294967168, 4294967232, 4294967264, 4294967280, 4294967288, 4294967292, 4294967294, 4294967295],
  ip_mask:: [4278190080, 16711680, 65280, 255],
};

local stringifyAddrBytes = function(bytes, mask=-1)
  local str = '' + ((bytes & const.ip_mask[0]) / const.pow2[24]) +
              '.' + ((bytes & const.ip_mask[1]) / const.pow2[16]) +
              '.' + ((bytes & const.ip_mask[2]) / const.pow2[8]) +
              '.' + (bytes & const.ip_mask[3]);
  if mask == -1 then str
  else str + '/' + mask;

local debug = function(x) std.trace('debug: ' + x, x);

local ipv4 = {
  /**
   * @param addr  Object representing an IPv4 address
   * @return True if the given IPv4 object is a valid ipv4.addr, otherwise false
   */
  /**
   * Returns true if the given parameter is a valid ipv4.addr object.
   */
  isAddrObject(addr)::
    std.isObject(addr) &&
    std.objectHasAll(addr, 'short') && std.objectHasAll(addr, 'full') &&
    std.objectHasAll(addr, 'bytes') && std.objectHasAll(addr, 'mask') &&
    addr.bytes >= 0 && addr.bytes < const.pow2[32] &&
    addr.mask >= 0 && addr.mask <= 32,

  /**
   * @param addr IP address, which can be a string or an integer (sum of the IP bytes)
   * @param mask IP mask, which is ignored if the previous parameter contains the mask
   * @return A json representation of the IP address
   *    - short:  String representation of the address, without mask (hidden)
   *    - full:   String representation of the address, with mask
   *    - bytes:  Integer value of the address (sum of all bytes) (hidden)
   *    - mask:   Integer value of the address mask (hidden)
   */
  /**
   * Parse an IPv4 address and returns an object containing information about it.
   */
  addr(addr, mask=-1)::
    (
      assert std.isString(addr) || std.isNumber(addr) : 'ipv4.addr first parameter must be a number or a string';
      if std.isNumber(addr)
      then
        assert addr >= 0 && addr < const.pow2[32] : 'ipv4.addr first parameter must be a valid IPv4 address (not %s)' % addr;
        assert mask >= 0 && mask <= 32 : 'ipv4.addr second parameter must be a valid IPv4 mask (%s not in [0, 32])' % mask;

        {
          bytes:: addr,
          mask:: mask,
        }
      else
        local saddr = std.split(addr, '/');
        local sbytes = std.split(saddr[0], '.');
        local bytes = [std.parseInt(sbytes[0]), std.parseInt(sbytes[1]), std.parseInt(sbytes[2]), std.parseInt(sbytes[3])];
        local netmask = (if std.length(saddr) == 1 then (if mask == -1 then 32 else mask) else std.parseInt(saddr[1]));

        assert std.length(saddr) < 3 && std.length(sbytes) == 4 : 'ipv4.addr first parameter must be a valid IPv4 address (not %s)' % saddr;
        assert bytes[0] >= 0 && bytes[0] <= 255 : 'ipv4.addr first parameter must be a valid IPv4 address (invalid first byte "%s")' % bytes[0];
        assert bytes[1] >= 0 && bytes[1] <= 255 : 'ipv4.addr first parameter must be a valid IPv4 address (invalid second byte "%s")' % bytes[1];
        assert bytes[2] >= 0 && bytes[2] <= 255 : 'ipv4.addr first parameter must be a valid IPv4 address (invalid third byte "%s")' % bytes[2];
        assert bytes[3] >= 0 && bytes[3] <= 255 : 'ipv4.addr first parameter must be a valid IPv4 address (invalid fourth byte "%s")' % bytes[3];
        assert netmask >= 0 && netmask <= 32 : (if std.length(saddr) == 1 then 'ipv4.addr first parameter must be a valid IPv4 address (invalid mask for %s)' % addr
                                                else 'ipv4.addr second parameter must be a valid IPv4 mask (%s not in [0, 32])' % netmask);

        {
          bytes:: bytes[0] * const.pow2[24] + bytes[1] * const.pow2[16] + bytes[2] * const.pow2[8] + bytes[3],
          mask:: netmask,
        }
    ) {
      short:: stringifyAddrBytes(self.bytes),
      full: stringifyAddrBytes(self.bytes, self.mask),
    },

  /**
   * @param addr              IP address, which can be a string or an integer (sum of the IP bytes)
   * @param mask              IP mask, which is ignored if the previous parameter contains the mask
   * @param ignore_validation Ignore all verification, for internal usage
   * @return A json representation of the IPv4 subnet
   *    - short:      String representation of the address, without mask (hidden)
   *    - full:       String representation of the address, with mask (hidden)
   *    - bytes:      Integer value of the address (sum of all bytes) (hidden)
   *    - mask:       Integer value of the address mask
   *
   *    - address:    String representation of the address, with mask
   *    - broadcast:  IPv4 address of the broadcast (string)
   *    - netmask:    Human readable netmask (string)
   *    - wildcard:   Inverse of netmask
   *    - hostMin:    IPv4 of the first subnet host (string)
   *    - hostMax:    IPv4 of the last subnet host (string)
   *    - numHost:    Number of possible host in the subnet
   */
  /**
   * Parse an IPv4 address as subnet and returns an object containing information about it.
   */
  subnet(addr, mask=-1, ignore_validation=false)::
    assert std.isNumber(addr) || std.isString(addr) || std.isObject(addr) : 'ipv4.subnet first parameter must be a number or a string';
    (
      if ignore_validation then
        addr
      else if std.isNumber(addr) then
        assert std.isNumber(mask) : 'ipv4.subnet first parameter must be a valid IPv4 address';
        assert addr >= 0 && addr <= const.pow2[32] && mask >= 0 && mask <= 32 : 'ipv4.subnet first parameter must be a valid IPv4 address';
        {
          bytes:: addr,
          mask:: mask,
          short:: stringifyAddrBytes(self.bytes),
          full: stringifyAddrBytes(self.bytes, self.mask),
        }
      else if std.isString(addr) then
        ipv4.addr(addr, mask)
      else
        assert ipv4.isAddrObject(addr) : 'ipv4.subnet first parameter must be a valid IPv4 address object';
        addr
    ) {
      bytes:: super.bytes & const.mask[self.mask],
      full:: super.full,

      address::: self.full,
      mask::: super.mask,

      broadcast: (
        if self.mask > 30 then null
        else if self.mask == 0 then '255.255.255.255'
        else stringifyAddrBytes(self.next().bytes - 1)
      ),
      netmask: stringifyAddrBytes(const.mask[self.mask]),
      wildcard: stringifyAddrBytes(const.pow2[32 - self.mask] - 1),

      hostMin: (
        if self.mask > 30 then self.short
        else stringifyAddrBytes(self.bytes + 1)
      ),
      hostMax: (
        if self.mask == 32 then self.short
        else if self.mask == 31 then stringifyAddrBytes(self.bytes + 1)
        else if self.mask == 0 then '255.255.255.254'
        else stringifyAddrBytes(self.next().bytes - 2)
      ),
      numHost: (
        if self.mask == 32 then 1
        else if self.mask == 31 then 2
        else const.pow2[32 - self.mask] - 2
      ),

      /**
       * @param nth  The index of the internal subnet IP
       * @return A json representation of the nth IP address
       */
      /**
       * Return the IPv4 address of the nth 'host' in the subnet.
       */
      ip(nth=0)::
        assert nth < self.numHost : 'subnet in /%s cannot have more than %d addresses' % [self.mask, self.numHost];
        ipv4.addr(self.bytes + nth, self.mask),

      /**
       * @param index  The index of the previous subnet
       * @return A json representation of the previous nth subnet
       */
      /**
       * Return the subnet object of the previous nth subnet, with the same mask.
       */
      prev(index=1):: self.next(-index),

      /**
       * @param index  The index of the next subnet
       * @return A json representation of the next nth subnet
       */
      /**
       * Return the subnet object of the next nth subnet, with the same mask.
       */
      next(index=1)::
        local subnet = self;
        local addr = {
          bytes:: subnet.bytes + const.pow2[32 - self.mask] * index,
          mask:: subnet.mask,
          short:: stringifyAddrBytes(self.bytes),
          full: stringifyAddrBytes(self.bytes, self.mask),
        };
        ipv4.subnet(addr, null, true),

      /**
       * @param subnet_addr The child network IPv4 address
       * @return True if the current subnet is contained in the given network, otherwise false
       */
      /**
       * Check if the current subnet contains the given subnet address.
       */
      hasSubnet(subnet_addr)::
        local addr =
          if std.isString(subnet_addr) then ipv4.addr(subnet_addr)
          else if ipv4.isAddrObject(subnet_addr) then subnet_addr
          else error 'ipv4.subnet.hasSubnet parameter must be a string or an ipv4.addr object';
        self.mask <= addr.mask && self.bytes == ipv4.subnet(addr.bytes, self.mask).bytes,

      /**
       * @param mask  The mask of the subnet to known
       * @return The number of subnet inside the current subnet with the given mask
       */
      /**
       * Return the number of subnet inside the current subnet with the given mask.
       */
      numSubnet(mask)::
        if !std.isNumber(mask) then error 'ipv4.subnet.numSubnet parameter must be a number'
        else if self.mask > mask then 'ipv4.subnet.numSubnet parameter must be greater or equal to the current subnet mask (%d)' % self.mask
        else const.pow2[mask - self.mask],

      /**
       * @param mask  The mask of the subnet used to split the current subnet
       * @return The first subnet with inside the current subnet with the given mask
       */
      /**
       * Divide the current subnet in smallest subnet with the given mask. It returns the first
       * subnet of them, so you can iterate over them thanks to the next and prev methods.
       */
      divide(mask)::
        if !std.isNumber(mask) then error 'ipv4.subnet.numSubnet parameter must be a number'
        else if self.mask > mask then 'ipv4.subnet.numSubnet parameter must be greater or equal to the current subnet mask (%d)' % self.mask
        else ipv4.subnet(self.bytes, mask),
    },
};

ipv4 {
  const:: {
    pow2:: const.pow2,
    mask:: const.mask,
  },
}
